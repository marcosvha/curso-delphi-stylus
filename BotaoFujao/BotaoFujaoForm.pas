unit BotaoFujaoForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmBotaoFujao = class(TForm)
    Button: TButton;
    procedure ButtonMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBotaoFujao: TfmBotaoFujao;

implementation

{$R *.dfm}

procedure TfmBotaoFujao.ButtonMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  // fmBotaoFujao.Width = largura do form em Pixels
  // Button.Width       = largura do bot�o em Pixels
  // Random             = N�mero aleat�rio de 0 at� o valor entre ()
  Button.Left:= Random(fmBotaoFujao.Width - Button.Width);
  Button.Top := Random(fmBotaoFujao.Height - Button.Height);
end;

procedure TfmBotaoFujao.ButtonClick(Sender: TObject);
begin
  ShowMessage('Parab�ns!'); 
  Close;
end;

end.

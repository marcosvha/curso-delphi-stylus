unit CalculadoraForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmCalculadora = class(TForm)
    edValor1: TEdit;
    edValor2: TEdit;
    edResultado: TEdit;
    Label1: TLabel;
    btCalcular: TButton;
    btLimpar: TButton;
    procedure btCalcularClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCalculadora: TfmCalculadora;

implementation

{$R *.dfm}

procedure TfmCalculadora.btCalcularClick(Sender: TObject);
var
  rValor1, rValor2: Real;
begin
  //Atribui os textos dos Edits para vari�veis
  rValor1:= StrToFloat(edValor1.Text); //StrToFloat: Transforma Texto para Valor
  rValor2:= StrToFloat(edValor2.Text);
  edResultado.Text:= FloatToStr(rValor1 * rValor2); //FloatToStr: Transforma Valor para Texto
end;

procedure TfmCalculadora.btLimparClick(Sender: TObject);
begin
  edValor1.Text:= '0'; //poderia usar o m�todo edValor1.Clear
  edValor2.Text:= '0';
  edResultado.Text:= '0';

end;

end.

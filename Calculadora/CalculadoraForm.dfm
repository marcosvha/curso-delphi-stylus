object fmCalculadora: TfmCalculadora
  Left = 175
  Top = 175
  BorderStyle = bsSingle
  Caption = 'Calculadora'
  ClientHeight = 98
  ClientWidth = 522
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 144
    Top = 16
    Width = 13
    Height = 20
    Caption = 'X'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edValor1: TEdit
    Left = 16
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '0'
  end
  object edValor2: TEdit
    Left = 166
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '0'
  end
  object edResultado: TEdit
    Left = 384
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '0'
  end
  object btCalcular: TButton
    Left = 296
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Calcular'
    Default = True
    TabOrder = 3
    OnClick = btCalcularClick
  end
  object btLimpar: TButton
    Left = 224
    Top = 56
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Limpar'
    TabOrder = 4
    OnClick = btLimparClick
  end
end

object fmMaskEdits: TfmMaskEdits
  Left = 235
  Top = 109
  BorderStyle = bsSingle
  Caption = 'Mask Edits'
  ClientHeight = 290
  ClientWidth = 498
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 90
    Height = 13
    Caption = 'Informe a m'#225'scara:'
  end
  object Label2: TLabel
    Left = 56
    Top = 107
    Width = 26
    Height = 13
    Caption = 'Data:'
  end
  object Label3: TLabel
    Left = 56
    Top = 139
    Width = 23
    Height = 13
    Caption = 'CPF:'
  end
  object Label4: TLabel
    Left = 56
    Top = 171
    Width = 30
    Height = 13
    Caption = 'CNPJ:'
  end
  object Label5: TLabel
    Left = 56
    Top = 203
    Width = 48
    Height = 13
    Caption = 'Matr'#237'cula:'
  end
  object Labelx: TLabel
    Left = 56
    Top = 235
    Width = 34
    Height = 13
    Caption = 'Senha:'
  end
  object MaskEdit: TMaskEdit
    Left = 360
    Top = 15
    Width = 116
    Height = 21
    EditMask = '!99/99/00;1;_'
    MaxLength = 8
    TabOrder = 0
    Text = '  /  /  '
  end
  object cbMascaras: TComboBox
    Left = 112
    Top = 13
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
  end
  object btAplicar: TButton
    Left = 269
    Top = 12
    Width = 75
    Height = 25
    Caption = 'Aplicar >>'
    TabOrder = 2
    OnClick = btAplicarClick
  end
  object meData: TMaskEdit
    Left = 114
    Top = 104
    Width = 111
    Height = 21
    EditMask = '90/90/9900;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit
    Left = 114
    Top = 136
    Width = 113
    Height = 21
    EditMask = '999.999.999-99;0;_'
    MaxLength = 14
    TabOrder = 4
  end
  object MaskEdit4: TMaskEdit
    Left = 114
    Top = 168
    Width = 113
    Height = 21
    EditMask = '99.999.999/9999-99;0;_'
    MaxLength = 18
    TabOrder = 5
  end
  object MaskEdit5: TMaskEdit
    Left = 114
    Top = 200
    Width = 113
    Height = 21
    EditMask = '9999999 9;0; '
    MaxLength = 9
    TabOrder = 6
  end
  object meSenha: TMaskEdit
    Left = 114
    Top = 232
    Width = 113
    Height = 21
    PasswordChar = '*'
    TabOrder = 7
  end
  object btTestarSenha: TButton
    Left = 232
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Testar Senha'
    TabOrder = 8
    OnClick = btTestarSenhaClick
  end
  object btTestarData: TButton
    Left = 232
    Top = 102
    Width = 75
    Height = 25
    Caption = 'Testar Data'
    TabOrder = 9
    OnClick = btTestarDataClick
  end
end

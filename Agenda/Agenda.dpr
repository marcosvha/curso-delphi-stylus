program Agenda;

uses
  Forms,
  ContatosForm in 'ContatosForm.pas' {fmContatos},
  AgendaForm in 'AgendaForm.pas' {fmAgenda},
  SplashForm in 'SplashForm.pas' {fmSplash},
  SobreForm in 'SobreForm.pas' {fmSobre},
  ContatosManForm in 'ContatosManForm.pas' {fmContatosMan},
  CompromissosForm in 'CompromissosForm.pas' {fmCompromissos};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmAgenda, fmAgenda);
  Application.CreateForm(TfmSplash, fmSplash);
  Application.CreateForm(TfmSobre, fmSobre);
  Application.CreateForm(TfmContatosMan, fmContatosMan);
  Application.CreateForm(TfmCompromissos, fmCompromissos);
  Application.Run;
end.

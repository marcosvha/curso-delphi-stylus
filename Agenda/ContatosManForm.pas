unit ContatosManForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, ComCtrls;

type
  TfmContatosMan = class(TForm)
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edNome: TEdit;
    edEndereco: TEdit;
    edCidade: TEdit;
    edCEP: TMaskEdit;
    edFone: TEdit;
    edEMail: TEdit;
    edDataNasc: TDateTimePicker;
    OK: TButton;
    Cancelar: TButton;
    cbUF: TComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmContatosMan: TfmContatosMan;

implementation

{$R *.dfm}

end.

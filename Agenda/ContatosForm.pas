{Inserir um objeto Table, associar o Database, dois clicks, Add all fields,
arrastar os campos para onde deseja inserir}
unit ContatosForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Mask, DBCtrls, DBTables, ComCtrls, Grids, DBGrids,
  ExtCtrls;

type
  TfmContatos = class(TForm)
    tbContatos: TTable;
    tbContatosCODIGO: TAutoIncField;
    tbContatosNOME: TStringField;
    tbContatosENDERECO: TStringField;
    tbContatosCIDADE: TStringField;
    tbContatosUF: TStringField;
    tbContatosCEP: TStringField;
    tbContatosTELEFONE: TStringField;
    tbContatosEMAIL: TStringField;
    tbContatosDATANASC: TDateField;
    dsContatos: TDataSource;
    DBGrid1: TDBGrid;
    Inclur: TButton;
    Alterar: TButton;
    Excluir: TButton;
    edNome: TEdit;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure InclurClick(Sender: TObject);
    procedure AlterarClick(Sender: TObject);
    procedure ExcluirClick(Sender: TObject);
    procedure edNomeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmContatos: TfmContatos;

implementation

uses ContatosManForm;

{$R *.dfm}

procedure TfmContatos.FormShow(Sender: TObject);
begin
  tbContatos.Open; //abre a tabela
end;

procedure TfmContatos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  tbContatos.Close; //fecha a tabela
end;

procedure TfmContatos.InclurClick(Sender: TObject);
begin
  fmContatosMan:= TfmContatosMan.Create(Self); //--> aloca mem�ria
  if fmContatosMan.ShowModal = mrOK then       //--> se o bot�o OK for pressonado
  begin
    tbContatos.Insert;                         //coloca a tabela em estado de Inclus�o

    //Atribui os valores informados pelo usu�rio aos campos da tabela
    tbContatos.FieldByName('NOME').AsString       := fmContatosMan.edNome.Text;
    tbContatos.FieldByName('ENDERECO').AsString   := fmContatosMan.edEndereco.Text;
    tbContatos.FieldByName('Cidade').AsString     := fmContatosMan.edCidade.Text;
    tbContatos.FieldByName('CEP').AsString        := fmContatosMan.edCEP.Text;
    tbContatos.FieldByName('Telefone').AsString       := fmContatosMan.edFone.Text;
    tbContatos.FieldByName('EMail').AsString      := fmContatosMan.edEMail.Text;
    tbContatos.FieldByName('DataNasc').AsDateTime := fmContatosMan.edDataNasc.Date;
    tbContatos.FieldByName('UF').AsString         := fmContatosMan.cbUF.Text;

    tbContatos.Post; //--> grava os dados para a tabela
  end;

end;

procedure TfmContatos.AlterarClick(Sender: TObject);
begin
  fmContatosMan:= TfmContatosMan.Create(Self);

  fmContatosMan.edNome.Text    := tbContatos.FieldByName('NOME').AsString;
  fmContatosMan.edEndereco.Text:= tbContatos.FieldByName('ENDERECO').AsString;
  fmContatosMan.edCidade.Text  := tbContatos.FieldByName('Cidade').AsString;
  fmContatosMan.edCEP.Text     := tbContatos.FieldByName('CEP').AsString;
  fmContatosMan.edFone.Text    := tbContatos.FieldByName('Telefone').AsString;
  fmContatosMan.edEMail.Text   := tbContatos.FieldByName('EMail').AsString;
  fmContatosMan.edDataNasc.Date:= tbContatos.FieldByName('DataNasc').AsDateTime;
  fmContatosMan.cbUF.ItemIndex := fmContatosMan.cbUF.Items.IndexOf(tbContatos.FieldByName('UF').AsString);

  if fmContatosMan.ShowModal = mrOK then
  begin
    tbContatos.Edit; //--> coloca a tabela (registro posicionado) em estado de Edi��o

    //Atribui os valores informados pelo usu�rio aos campos da tabela
    tbContatos.FieldByName('NOME').AsString       := fmContatosMan.edNome.Text;
    tbContatos.FieldByName('ENDERECO').AsString   := fmContatosMan.edEndereco.Text;
    tbContatos.FieldByName('Cidade').AsString     := fmContatosMan.edCidade.Text;
    tbContatos.FieldByName('CEP').AsString        := fmContatosMan.edCEP.Text;
    tbContatos.FieldByName('Fone').AsString       := fmContatosMan.edFone.Text;
    tbContatos.FieldByName('EMail').AsString      := fmContatosMan.edEMail.Text;
    tbContatos.FieldByName('DataNasc').AsDateTime := fmContatosMan.edDataNasc.Date;
    tbContatos.FieldByName('UF').AsString         := fmContatosMan.cbUF.Text;

    tbContatos.Post; //--> grava os dados para a tabela
  end;

end;

procedure TfmContatos.ExcluirClick(Sender: TObject);
begin
  if MessageDlg('Deseja realmente excluir o contato selecionado?', mtConfirmation,
                [mbYes, mbNo], 0) = mrYes then //--> se responder Sim � pergunta
    tbContatos.Delete; //--> exclui o registro posicionado

end;

procedure TfmContatos.edNomeKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {este comando localiza o valor digitado no edit edNome no campo Nome da
   tabela Contatos, a cada letra pressionada}
  tbContatos.Locate('nome',              //--> campo a procurar
                    edNome.Text,         //--> valor a procurar no campo
                    [loCaseInsensitive,  //--> ignora mai�sculas/min�sculas
                    loPartialKey]);      //--> busca parcial. ex.: "Mar"
                                         //posiciona no primeiro nome que come�a
                                         //com "Mar" (Marcos, Marcelo, Maria...)  
end;

end.

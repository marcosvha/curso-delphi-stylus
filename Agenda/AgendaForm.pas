{Form principal da aplica��o Agenda
  Realiza a configura��o do Alias via programa��o e cont�m as chamadas de menu
  para as outras telas. 
}
unit AgendaForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBTables, Menus, DB, ExtCtrls;

type
  TfmAgenda = class(TForm)
    MainMenu1: TMainMenu;
    Manuteno1: TMenuItem;
    Contatos1: TMenuItem;
    Compromissos1: TMenuItem;
    SobreaAgenda1: TMenuItem;
    Timer1: TTimer;
    tbCompromissos: TTable;
    tbCompromissosCODIGO: TAutoIncField;
    tbCompromissosDATA: TDateField;
    tbCompromissosHORA: TTimeField;
    tbCompromissosASSUNTO: TStringField;
    tbCompromissosCOD_CONTATO: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure Contatos1Click(Sender: TObject);
    procedure SobreaAgenda1Click(Sender: TObject);
    procedure Compromissos1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    UltimoAlarme: TDateTime;
  end;

var
  fmAgenda: TfmAgenda;

implementation

uses SplashForm, ContatosForm, SobreForm, CompromissosForm;

{$R *.dfm}

procedure TfmAgenda.FormShow(Sender: TObject);
begin
  fmSplash:= TfmSplash.Create(Self); //--> aloca mem�ria para o objeto fmSplash
  fmSplash.Show;                     //--> exibe o form
  fmSplash.ProgressBar.Position:= 10;

  fmSplash.Update;                   //--> atualiza o que � exibido no form
  Sleep(1000);                       //--> "adormece" por 1 segundo

  Session.DeleteAlias('Agenda');     //--> remove o alias, caso exista
  Session.AddStandardAlias('Agenda', ExtractFileDir(ParamStr(0)) + '\Dados', 'Paradox'); //--> cria novamente o alias com a configura��o correta
  Session.SaveConfigFile;            //--> salva as altera��es

  fmSplash.Close;                    //--> fecha a janela
  fmSplash.Free;                     //--> libera a mem�ria alocada para o objeto

end;

procedure TfmAgenda.Contatos1Click(Sender: TObject);
begin
  fmContatos:= TfmContatos.Create(Self); //--> aloca mem�ria para o objeto
  fmContatos.ShowModal; //--> exibe e aguarda o usu�rio fechar a janela para continuar
  fmContatos.Free;      //--> libera a mem�ria
end;

procedure TfmAgenda.SobreaAgenda1Click(Sender: TObject);
begin
  fmSobre:= TfmSobre.Create(Self); //--> aloca mem�ria para o objeto
  fmSobre.ShowModal; //--> exibe e aguarda o usu�rio fechar a janela para continuar
  fmSobre.Free;      //--> libera a mem�ria

end;

procedure TfmAgenda.Compromissos1Click(Sender: TObject);
begin
  fmCompromissos:= TfmCompromissos.Create(Self); //--> aloca mem�ria para o objeto
  fmCompromissos.ShowModal; //--> exibe e aguarda o usu�rio fechar a janela para continuar
  fmCompromissos.Free;      //--> libera a mem�ria
end;

procedure TfmAgenda.Timer1Timer(Sender: TObject);
var
  h, m, s, ms: word;
  Hora: TDateTime;
begin
  DecodeTime(Time, h, m, s, ms);
  Hora:= EncodeTime(H, M, 0, 0);
  if (tbCompromissos.Locate('data;hora', VarArrayOf([Date, Hora]), [])) and
     (Hora <> UltimoAlarme) then
  begin
    UltimoAlarme:= Hora;
    MessageDlg('Compromisso agendado para '+ DateToStr(Date) + ' ' +
               TimeToStr(Hora) + #13 +
               tbCompromissos.FieldByName('Assunto').AsString, mtWarning, [mbOK], 0);

  end;
end;

end.

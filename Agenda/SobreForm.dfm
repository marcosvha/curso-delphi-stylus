object fmSobre: TfmSobre
  Left = 236
  Top = 114
  BorderStyle = bsDialog
  Caption = 'Sobre a Agenda'
  ClientHeight = 204
  ClientWidth = 322
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 32
    Top = 32
    Width = 257
    Height = 105
    Lines.Strings = (
      'Programa criado para o curso da Stylus'
      ''
      'Por Marcos Henke'
      '')
    TabOrder = 0
  end
  object btOK: TButton
    Left = 112
    Top = 160
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    OnClick = btOKClick
  end
end

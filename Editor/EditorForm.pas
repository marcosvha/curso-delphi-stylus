unit EditorForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ToolWin, ImgList, StdCtrls, Buttons;

type
  TfmEditor = class(TForm)
    MainMenu1: TMainMenu;
    Novo1: TMenuItem;
    Novo2: TMenuItem;
    Abrir1: TMenuItem;
    Slvar1: TMenuItem;
    Salvarcomo1: TMenuItem;
    N1: TMenuItem;
    Imprimir1: TMenuItem;
    Configurarpgina1: TMenuItem;
    N2: TMenuItem;
    Sair1: TMenuItem;
    Editar1: TMenuItem;
    Recortar1: TMenuItem;
    Copiar1: TMenuItem;
    Colar1: TMenuItem;
    Limpar1: TMenuItem;
    Selecionartudo1: TMenuItem;
    N3: TMenuItem;
    Localizar1: TMenuItem;
    Localizarprxima1: TMenuItem;
    Formatar1: TMenuItem;
    Fonte1: TMenuItem;
    Pargrafo1: TMenuItem;
    ToolBar1: TToolBar;
    btNovo: TToolButton;
    btSalvar: TToolButton;
    btAbrir: TToolButton;
    ToolButton4: TToolButton;
    btImprimir: TToolButton;
    btCopiar: TToolButton;
    ToolButton7: TToolButton;
    btCortar: TToolButton;
    btColar: TToolButton;
    btLimpar: TToolButton;
    ToolButton14: TToolButton;
    btEsquerda: TToolButton;
    btCentro: TToolButton;
    btDireita: TToolButton;
    ImageList1: TImageList;
    btSublinhado: TSpeedButton;
    btItalico: TSpeedButton;
    btNegrito: TSpeedButton;
    RichEdit: TRichEdit;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    FontDialog: TFontDialog;
    FindDialog: TFindDialog;
    PrinterSetupDialog: TPrinterSetupDialog;
    PrintDialog: TPrintDialog;
    procedure btNovoClick(Sender: TObject);
    procedure btNegritoClick(Sender: TObject);
    procedure btItalicoClick(Sender: TObject);
    procedure btSublinhadoClick(Sender: TObject);
    procedure btEsquerdaClick(Sender: TObject);
    procedure btCentroClick(Sender: TObject);
    procedure btDireitaClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
    procedure btCopiarClick(Sender: TObject);
    procedure btCortarClick(Sender: TObject);
    procedure btColarClick(Sender: TObject);
    procedure Selecionartudo1Click(Sender: TObject);
    procedure btAbrirClick(Sender: TObject);
    procedure Salvarcomo1Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure Configurarpgina1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure Localizar1Click(Sender: TObject);
    procedure FindDialogFind(Sender: TObject);
    procedure Localizarprxima1Click(Sender: TObject);
    procedure Fonte1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmEditor: TfmEditor;

implementation

{$R *.dfm}

procedure TfmEditor.btNovoClick(Sender: TObject);
begin
  RichEdit.Clear; //limpa o texto atual
  RichEdit.PlainText:= False; //desativa a op��o texto plano
  SaveDialog.FileName:= 'Sem nome 1';  //atribui o nome padr�o ao SaveDialog
end;

procedure TfmEditor.btNegritoClick(Sender: TObject);
begin
  If fsBold in RichEdit.SelAttributes.Style then //se negrito est� no estilo...
    RichEdit.SelAttributes.Style := RichEdit.SelAttributes.Style - [fsBold]//retira
  else //caso contr�rio...
    RichEdit.SelAttributes.Style := RichEdit.SelAttributes.Style + [fsBold]; //coloca
end;

procedure TfmEditor.btItalicoClick(Sender: TObject);
begin
  If fsItalic in RichEdit.SelAttributes.Style then
    RichEdit.SelAttributes.Style := RichEdit.SelAttributes.Style - [fsItalic]
  else
    RichEdit.SelAttributes.Style := RichEdit.SelAttributes.Style + [fsItalic];

end;

procedure TfmEditor.btSublinhadoClick(Sender: TObject);
begin
  If fsUnderline in RichEdit.SelAttributes.Style then
    RichEdit.SelAttributes.Style := RichEdit.SelAttributes.Style - [fsUnderline]
  else
    RichEdit.SelAttributes.Style := RichEdit.SelAttributes.Style + [fsUnderline];

end;

procedure TfmEditor.btEsquerdaClick(Sender: TObject);
begin
  RichEdit.Paragraph.Alignment:= taLeftJustify;
end;

procedure TfmEditor.btCentroClick(Sender: TObject);
begin
  RichEdit.Paragraph.Alignment:= taCenter;
end;

procedure TfmEditor.btDireitaClick(Sender: TObject);
begin
  RichEdit.Paragraph.Alignment:= taRightJustify;
end;

procedure TfmEditor.btLimparClick(Sender: TObject);
begin
 RichEdit.ClearSelection; //apaga somente o que est� selecionado. igual a tecla DEL
end;

procedure TfmEditor.btCopiarClick(Sender: TObject);
begin
  RichEdit.CopyToClipboard;
end;

procedure TfmEditor.btCortarClick(Sender: TObject);
begin
  RichEdit.CutToClipboard;
end;

procedure TfmEditor.btColarClick(Sender: TObject);
begin
  RichEdit.PasteFromClipboard;
end;

procedure TfmEditor.Selecionartudo1Click(Sender: TObject);
begin
  RichEdit.SelectAll;

end;

procedure TfmEditor.btAbrirClick(Sender: TObject);
begin
  If OpenDialog.Execute then //se executar o di�logo...
  begin
    {4 passos em apenas uma linha de comando:
     1� - pega a extens�o do arquivo: ExtractFileExt(OpenDialog.FileName)
     2� - transforma esta extens�o para mai�sculas, pois o arquivo pode ter
          extens�es Rtf, RTF, rtf, Txt, txt, etc.: UpperCase
     3� - verifica se a extens�o em mai�sculas � diferente de '.RTF'
     4� - se for DIFERENTE, a propriedade PlainText (texto plano, sem formata��o)
          ser� VERDADEIRA, caso contr�rio, ser� FALSA
     }
    RichEdit.PlainText:= UpperCase(ExtractFileExt(OpenDialog.FileName)) <> '.RTF';

    //carrega o arquivo indicado no di�logo
    RichEdit.Lines.LoadFromFile(OpenDialog.FileName);
    //atribui o mesmo nome para o SaveDialog
    SaveDialog.FileName:= OpenDialog.FileName;
  end;
end;

procedure TfmEditor.Salvarcomo1Click(Sender: TObject);
begin
  If SaveDialog.Execute then //se executar o di�logo...
  begin
    If SaveDialog.FilterIndex = 1 then //se o �ndice do filtro for 1...
    begin
      ChangeFileExt(SaveDialog.FileName, '.rtf'); //troca a extens�o para rtf
      RichEdit.PlainText:= False; //desativa a op��o texto plano (sem formata��o)
    end
    else
    begin
      ChangeFileExt(SaveDialog.FileName, '.txt'); //troca a extens�o para txt
      RichEdit.PlainText:= True;  //desativa a op��o texto plano (sem formata��o)
    end;

    RichEdit.Lines.SaveToFile(SaveDialog.FileName);
  end;
end;

procedure TfmEditor.btSalvarClick(Sender: TObject);
begin
  If (SaveDialog.FileName <> 'Sem nome 1') and //se o nome � diferente do padr�o E
     (FileExists(SaveDialog.FileName)) then //se o arquivo j� existe...
    RichEdit.Lines.SaveToFile(SaveDialog.FileName) //salva direto, sem perguntar o nome
  else //caso contr�rio...
    Salvarcomo1Click(Self); //executa as linhas de comando do Salvar Como
end;

procedure TfmEditor.btImprimirClick(Sender: TObject);
begin
  If PrintDialog.Execute then
    RichEdit.Print(SaveDialog.FileName);
end;

procedure TfmEditor.Configurarpgina1Click(Sender: TObject);
begin
  PrinterSetupDialog.Execute;
end;

procedure TfmEditor.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmEditor.Localizar1Click(Sender: TObject);
begin
  FindDialog.Execute;
end;

procedure TfmEditor.FindDialogFind(Sender: TObject);
var
  FoundAt: LongInt;
  StartPos, ToEnd: Integer;
begin
  with RichEdit do //com o RichEdit fa�a:
  begin
    if SelLength <> 0 then //se algo est� selecionado...
      StartPos := SelStart + SelLength //o ponto inicial � a partir da palavra selecionada
    else //caso contr�rio...
      StartPos := 0; //inicia do in�cio do texto

    // ToEnd � a diferen�a do in�cio do texto para o final
    ToEnd := Length(Text) - StartPos;

    //Posi��o onde encontrou o texto � armazenada na vari�vel FoundAt
    FoundAt := FindText(FindDialog.FindText, StartPos, ToEnd, []);
    SetFocus;
    if FoundAt <> -1 then //Se a posi��o encontrada for <> -1...
    begin
      SelStart := FoundAt; //marca o in�cio da nova sele��o para o valor em FoundAt
      //marca o final para o final do texto pesquisado
      SelLength := Length(FindDialog.FindText);
    end; //fim do if
  end; //fim do with RichEdit
end;


procedure TfmEditor.Localizarprxima1Click(Sender: TObject);
begin
  FindDialogFind(self); //somente executa novamente a busca, n�o abre o di�logo
end;

procedure TfmEditor.Fonte1Click(Sender: TObject);
begin
  If FontDialog.Execute then
    RichEdit.SelAttributes.Assign(FontDialog.Font);


  //ou todas as linhas abaixo:

  {begin
    RichEdit.SelAttributes.Name:= FontDialog.Font.Name;
    RichEdit.SelAttributes.Style:= FontDialog.Font.Style;
    RichEdit.SelAttributes.Color:= FontDialog.Font.Color;
    RichEdit.SelAttributes.Size:= FontDialog.Font.Size;
  end;}
end;

end.

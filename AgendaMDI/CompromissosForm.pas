unit CompromissosForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, DBTables, Grids, DBGrids;

type
  TfmCompromissos = class(TForm)
    DBGrid1: TDBGrid;
    Inclur: TButton;
    Alterar: TButton;
    Excluir: TButton;
    tbCompromissos: TTable;
    dsCompromissos: TDataSource;
    edNome: TEdit;
    Label1: TLabel;
    tbCompromissosCODIGO: TAutoIncField;
    tbCompromissosDATA: TDateField;
    tbCompromissosHORA: TTimeField;
    tbCompromissosASSUNTO: TStringField;
    tbCompromissosCOD_CONTATO: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCompromissos: TfmCompromissos;

implementation

{$R *.dfm}

procedure TfmCompromissos.FormShow(Sender: TObject);
begin
  tbCompromissos.Open; //abre a tabela
end;

procedure TfmCompromissos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  tbCompromissos.Close; //fecha a tabela
  Action:= caFree;
end;

end.

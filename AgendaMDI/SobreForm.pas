unit SobreForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmSobre = class(TForm)
    Memo1: TMemo;
    btOK: TButton;
    procedure btOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSobre: TfmSobre;

implementation

{$R *.dfm}

procedure TfmSobre.btOKClick(Sender: TObject);
begin
  Close;
end;

end.

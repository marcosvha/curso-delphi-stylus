unit ContatosManForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, ComCtrls;

type
  TfmContatosMan = class(TForm)
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edNome: TEdit;
    edEndereco: TEdit;
    edCidade: TEdit;
    edCEP: TMaskEdit;
    edFone: TEdit;
    edEMail: TEdit;
    edDataNasc: TDateTimePicker;
    OK: TButton;
    Cancelar: TButton;
    cbUF: TComboBox;
    procedure OKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmContatosMan: TfmContatosMan;

implementation

uses ContatosForm;

{$R *.dfm}

procedure TfmContatosMan.OKClick(Sender: TObject);
begin
  //Atribui os valores informados pelo usu�rio aos campos da tabela.
  //A tabela j� deve estar em estado de Inclus�o ou Altera��o
  with fmContatos do //--> Com o form fmContatos fa�a:
  begin
    tbContatos.FieldByName('NOME').AsString       := Self.edNome.Text;
    tbContatos.FieldByName('ENDERECO').AsString   := edEndereco.Text;
    tbContatos.FieldByName('Cidade').AsString     := edCidade.Text;
    tbContatos.FieldByName('CEP').AsString        := edCEP.Text;
    tbContatos.FieldByName('Telefone').AsString   := edFone.Text;
    tbContatos.FieldByName('EMail').AsString      := edEMail.Text;
    tbContatos.FieldByName('DataNasc').AsDateTime := edDataNasc.Date;
    tbContatos.FieldByName('UF').AsString         := cbUF.Text;

    tbContatos.Post; //--> grava os dados para a tabela
  end;

  Close;
end;

procedure TfmContatosMan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:= caFree;
end;

procedure TfmContatosMan.CancelarClick(Sender: TObject);
begin
  fmContatos.tbContatos.Cancel; //--> cancela o estado de Inclus�o ou Altera��o
  Close;
end;

end.

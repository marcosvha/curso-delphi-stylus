{Inserir um objeto Table, associar o Database, dois clicks, Add all fields,
arrastar os campos para onde deseja inserir}
unit ContatosForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Mask, DBCtrls, DBTables, ComCtrls, Grids, DBGrids,
  ExtCtrls;

type
  TfmContatos = class(TForm)
    tbContatos: TTable;
    tbContatosCODIGO: TAutoIncField;
    tbContatosNOME: TStringField;
    tbContatosENDERECO: TStringField;
    tbContatosCIDADE: TStringField;
    tbContatosUF: TStringField;
    tbContatosCEP: TStringField;
    tbContatosTELEFONE: TStringField;
    tbContatosEMAIL: TStringField;
    tbContatosDATANASC: TDateField;
    dsContatos: TDataSource;
    DBGrid1: TDBGrid;
    Inclur: TButton;
    Alterar: TButton;
    Excluir: TButton;
    edNome: TEdit;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure InclurClick(Sender: TObject);
    procedure AlterarClick(Sender: TObject);
    procedure ExcluirClick(Sender: TObject);
    procedure edNomeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmContatos: TfmContatos;

implementation

uses ContatosManForm, AgendaForm;

{$R *.dfm}

procedure TfmContatos.FormShow(Sender: TObject);
begin
  tbContatos.Open; //abre a tabela
end;

procedure TfmContatos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  tbContatos.Close; //fecha a tabela
  Action:= caFree;
end;

procedure TfmContatos.InclurClick(Sender: TObject);
begin
  tbContatos.Insert;  //--> coloca a tabela Contatos em estado de Inclus�o
  if fmAgenda.NotFindForm(TfmContatosMan) then
  begin
    fmContatosMan:= TfmContatosMan.Create(Self); //--> aloca mem�ria
    fmContatosMan.Show;                          //--> exibe o form
  end
  else
    fmContatosMan.BringToFront;
end;

procedure TfmContatos.AlterarClick(Sender: TObject);
begin
  if fmAgenda.NotFindForm(TfmContatosMan) then
  begin
    fmContatosMan:= TfmContatosMan.Create(Self); //--> aloca mem�ria
    with fmContatosMan do
    begin
      edNome.Text:= tbContatos.FieldByName('NOME').AsString;
      edEndereco.Text:= tbContatos.FieldByName('ENDERECO').AsString;
      edCidade.Text  := tbContatos.FieldByName('Cidade').AsString;
      edCEP.Text     := tbContatos.FieldByName('CEP').AsString;
      edFone.Text    := tbContatos.FieldByName('Telefone').AsString;
      edEMail.Text   := tbContatos.FieldByName('EMail').AsString;
      edDataNasc.Date:= tbContatos.FieldByName('DataNasc').AsDateTime;
      cbUF.ItemIndex := fmContatosMan.cbUF.Items.IndexOf(tbContatos.FieldByName('UF').AsString);

      tbContatos.Edit;  //--> coloca a tabela Contatos em estado de Altera��o
      fmContatosMan.Show;                          //--> exibe o form
    end;
  end
  else
    fmContatosMan.BringToFront;
end;

procedure TfmContatos.ExcluirClick(Sender: TObject);
begin
  if MessageDlg('Deseja realmente excluir o contato selecionado?', mtConfirmation,
                [mbYes, mbNo], 0) = mrYes then //--> se responder Sim � pergunta
    tbContatos.Delete; //--> exclui o registro posicionado

end;

procedure TfmContatos.edNomeKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {este comando localiza o valor digitado no edit edNome no campo Nome da
   tabela Contatos, a cada letra pressionada}
  tbContatos.Locate('nome',              //--> campo a procurar
                    edNome.Text,         //--> valor a procurar no campo
                    [loCaseInsensitive,  //--> ignora mai�sculas/min�sculas
                    loPartialKey]);      //--> busca parcial. ex.: "Mar"
                                         //posiciona no primeiro nome que come�a
                                         //com "Mar" (Marcos, Marcelo, Maria...)  
end;

end.

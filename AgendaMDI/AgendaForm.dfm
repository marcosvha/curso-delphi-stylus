object fmAgenda: TfmAgenda
  Left = 236
  Top = 114
  Width = 696
  Height = 480
  Caption = 'fmAgenda'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MainMenu1
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 136
    Top = 88
    object Manuteno1: TMenuItem
      Caption = 'Manuten'#231#227'o'
      object Contatos1: TMenuItem
        Caption = 'Contatos...'
        OnClick = Contatos1Click
      end
      object Compromissos1: TMenuItem
        Caption = 'Compromissos...'
        OnClick = Compromissos1Click
      end
    end
    object SobreaAgenda1: TMenuItem
      Caption = 'Sobre a Agenda'
      OnClick = SobreaAgenda1Click
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 184
    Top = 88
  end
  object tbCompromissos: TTable
    Active = True
    DatabaseName = 'Agenda'
    IndexName = 'ind_DataHora'
    TableName = 'Compromissos.db'
    Left = 224
    Top = 88
    object tbCompromissosCODIGO: TAutoIncField
      FieldName = 'CODIGO'
      ReadOnly = True
    end
    object tbCompromissosDATA: TDateField
      FieldName = 'DATA'
    end
    object tbCompromissosHORA: TTimeField
      FieldName = 'HORA'
    end
    object tbCompromissosASSUNTO: TStringField
      FieldName = 'ASSUNTO'
      Size = 50
    end
    object tbCompromissosCOD_CONTATO: TIntegerField
      FieldName = 'COD_CONTATO'
    end
  end
end

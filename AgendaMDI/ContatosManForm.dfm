object fmContatosMan: TfmContatosMan
  Left = 310
  Top = 134
  BorderStyle = bsDialog
  Caption = 'fmContatosMan'
  ClientHeight = 367
  ClientWidth = 338
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 5
    Width = 28
    Height = 13
    Caption = 'Nome'
    FocusControl = edNome
  end
  object Label3: TLabel
    Left = 8
    Top = 45
    Width = 46
    Height = 13
    Caption = 'Endere'#231'o'
    FocusControl = edEndereco
  end
  object Label4: TLabel
    Left = 8
    Top = 85
    Width = 33
    Height = 13
    Caption = 'Cidade'
    FocusControl = edCidade
  end
  object Label5: TLabel
    Left = 8
    Top = 126
    Width = 14
    Height = 13
    Caption = 'UF'
  end
  object Label6: TLabel
    Left = 8
    Top = 165
    Width = 21
    Height = 13
    Caption = 'CEP'
    FocusControl = edCEP
  end
  object Label7: TLabel
    Left = 8
    Top = 205
    Width = 42
    Height = 13
    Caption = 'Telefone'
    FocusControl = edFone
  end
  object Label8: TLabel
    Left = 8
    Top = 245
    Width = 29
    Height = 13
    Caption = 'E-Mail'
    FocusControl = edEMail
  end
  object Label9: TLabel
    Left = 8
    Top = 285
    Width = 82
    Height = 13
    Caption = 'Data Nascimento'
    FocusControl = edDataNasc
  end
  object edNome: TEdit
    Left = 8
    Top = 21
    Width = 305
    Height = 21
    TabOrder = 0
  end
  object edEndereco: TEdit
    Left = 8
    Top = 61
    Width = 305
    Height = 21
    TabOrder = 1
  end
  object edCidade: TEdit
    Left = 8
    Top = 101
    Width = 305
    Height = 21
    TabOrder = 2
  end
  object edCEP: TMaskEdit
    Left = 8
    Top = 181
    Width = 108
    Height = 21
    TabOrder = 3
  end
  object edFone: TEdit
    Left = 8
    Top = 221
    Width = 199
    Height = 21
    TabOrder = 4
  end
  object edEMail: TEdit
    Left = 8
    Top = 261
    Width = 305
    Height = 21
    TabOrder = 5
  end
  object edDataNasc: TDateTimePicker
    Left = 8
    Top = 301
    Width = 97
    Height = 21
    CalAlignment = dtaLeft
    Date = 37722.9564920023
    Time = 37722.9564920023
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 6
  end
  object OK: TButton
    Left = 80
    Top = 336
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 7
    OnClick = OKClick
  end
  object Cancelar: TButton
    Left = 176
    Top = 336
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancelar'
    ModalResult = 2
    TabOrder = 8
    OnClick = CancelarClick
  end
  object cbUF: TComboBox
    Left = 8
    Top = 141
    Width = 43
    Height = 21
    ItemHeight = 13
    TabOrder = 9
  end
end

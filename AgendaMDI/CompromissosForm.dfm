object fmCompromissos: TfmCompromissos
  Left = 100
  Top = 172
  Width = 696
  Height = 239
  Caption = 'fmCompromissos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    688
    205)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 90
    Height = 13
    Caption = 'Pesquisar Assunto:'
  end
  object DBGrid1: TDBGrid
    Left = 7
    Top = 48
    Width = 681
    Height = 110
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsCompromissos
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODIGO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HORA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ASSUNTO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'COD_CONTATO'
        Visible = True
      end>
  end
  object Inclur: TButton
    Left = 8
    Top = 167
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Inclur'
    TabOrder = 1
  end
  object Alterar: TButton
    Left = 96
    Top = 167
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Alterar'
    TabOrder = 2
  end
  object Excluir: TButton
    Left = 184
    Top = 167
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Excluir'
    TabOrder = 3
  end
  object edNome: TEdit
    Left = 111
    Top = 13
    Width = 202
    Height = 21
    TabOrder = 4
  end
  object tbCompromissos: TTable
    DatabaseName = 'Agenda'
    IndexName = 'ind_DataHora'
    TableName = 'Compromissos.db'
    Left = 296
    Top = 344
    object tbCompromissosCODIGO: TAutoIncField
      FieldName = 'CODIGO'
      ReadOnly = True
    end
    object tbCompromissosDATA: TDateField
      FieldName = 'DATA'
    end
    object tbCompromissosHORA: TTimeField
      FieldName = 'HORA'
    end
    object tbCompromissosASSUNTO: TStringField
      FieldName = 'ASSUNTO'
      Size = 50
    end
    object tbCompromissosCOD_CONTATO: TIntegerField
      FieldName = 'COD_CONTATO'
    end
  end
  object dsCompromissos: TDataSource
    DataSet = tbCompromissos
    Left = 328
    Top = 344
  end
end

object fmSplash: TfmSplash
  Left = 288
  Top = 161
  BorderStyle = bsNone
  Caption = 'fmSplash'
  ClientHeight = 188
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 168
    Top = 80
    Width = 94
    Height = 13
    Caption = 'Aguarde... iniciando'
  end
  object ProgressBar: TProgressBar
    Left = 48
    Top = 120
    Width = 329
    Height = 17
    Min = 0
    Max = 100
    TabOrder = 0
  end
end

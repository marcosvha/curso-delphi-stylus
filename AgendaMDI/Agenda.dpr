program Agenda;

uses
  Forms,
  SplashForm in 'SplashForm.pas' {fmSplash},
  AgendaForm in 'AgendaForm.pas' {fmAgenda},
  CompromissosForm in 'CompromissosForm.pas' {fmCompromissos},
  ContatosForm in 'ContatosForm.pas' {fmContatos},
  ContatosManForm in 'ContatosManForm.pas' {fmContatosMan},
  SobreForm in 'SobreForm.pas' {fmSobre};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmAgenda, fmAgenda);
  Application.Run;
end.

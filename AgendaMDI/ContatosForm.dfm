object fmContatos: TfmContatos
  Left = 82
  Top = 158
  Width = 633
  Height = 246
  Caption = 'Contatos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    625
    212)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 80
    Height = 13
    Caption = 'Pesquisar Nome:'
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 48
    Width = 606
    Height = 114
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsContatos
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODIGO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENDERECO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CEP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TELEFONE'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EMAIL'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATANASC'
        Visible = True
      end>
  end
  object Inclur: TButton
    Left = 8
    Top = 169
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Inclur'
    TabOrder = 1
    OnClick = InclurClick
  end
  object Alterar: TButton
    Left = 96
    Top = 169
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Alterar'
    TabOrder = 2
    OnClick = AlterarClick
  end
  object Excluir: TButton
    Left = 184
    Top = 169
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Excluir'
    TabOrder = 3
    OnClick = ExcluirClick
  end
  object edNome: TEdit
    Left = 95
    Top = 13
    Width = 202
    Height = 21
    TabOrder = 4
    OnKeyUp = edNomeKeyUp
  end
  object tbContatos: TTable
    DatabaseName = 'Agenda'
    TableName = 'Contatos.DB'
    Left = 296
    Top = 344
    object tbContatosCODIGO: TAutoIncField
      FieldName = 'CODIGO'
      ReadOnly = True
    end
    object tbContatosNOME: TStringField
      FieldName = 'NOME'
      Size = 50
    end
    object tbContatosENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object tbContatosCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 30
    end
    object tbContatosUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
    object tbContatosCEP: TStringField
      FieldName = 'CEP'
      EditMask = '99999-999;0; '
      Size = 8
    end
    object tbContatosTELEFONE: TStringField
      FieldName = 'TELEFONE'
      Size = 15
    end
    object tbContatosEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 30
    end
    object tbContatosDATANASC: TDateField
      FieldName = 'DATANASC'
    end
  end
  object dsContatos: TDataSource
    DataSet = tbContatos
    Left = 328
    Top = 344
  end
end

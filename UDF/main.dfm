object fmMain: TfmMain
  Left = 183
  Top = 189
  Width = 534
  Height = 341
  Caption = 'fmMain'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btIP: TButton
    Left = 24
    Top = 24
    Width = 75
    Height = 25
    Caption = 'IP'
    TabOrder = 0
    OnClick = btIPClick
  end
  object btSobrenome: TButton
    Left = 24
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Sobrenome'
    TabOrder = 1
    OnClick = btSobrenomeClick
  end
  object btValorExtenso: TButton
    Left = 24
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Valor extenso'
    TabOrder = 2
    OnClick = btValorExtensoClick
  end
  object btNomeComp: TButton
    Left = 112
    Top = 24
    Width = 97
    Height = 25
    Caption = 'Nome computador'
    TabOrder = 3
    OnClick = btNomeCompClick
  end
  object btNomeUsuario: TButton
    Left = 224
    Top = 24
    Width = 97
    Height = 25
    Caption = 'Nome usu'#225'rio'
    TabOrder = 4
    OnClick = btNomeUsuarioClick
  end
  object btPriMaiuscula: TButton
    Left = 112
    Top = 64
    Width = 97
    Height = 25
    Caption = 'Primeira Mai'#250'scula'
    TabOrder = 5
    OnClick = btPriMaiusculaClick
  end
  object btRetiraAcento: TButton
    Left = 224
    Top = 64
    Width = 97
    Height = 25
    Caption = 'Retira acentos'
    TabOrder = 6
    OnClick = btRetiraAcentoClick
  end
end

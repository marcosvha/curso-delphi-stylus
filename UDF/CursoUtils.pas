unit CursoUtils;

interface

uses
  SysUtils, Types, Windows, WinSock; //OBS: essas s�o units padr�es do delphi utilizadas nas fun��es abaixo

type
  TCharSet = set of Char;

  function Extenso(Valor: string; MF: string = 'M'): string;
  function Iff(Expr: Boolean; ResultTrue: variant; ResultFalse: variant): variant;
  function IPLocal(NomeDoComputador :String) :String ;
  function NomeComputador :String ;
  function NomeUsuario :String;
  function PrimeiraMaiuscula(const S: string; const WordDelims: TCharSet): string;
  function RetiraAcentuacao(Value: string): string;
  function SobreNome(Palavra: string): string;
  function StrZero(Valor: string; TamanhoCampo: Integer; QtdeZerosDecimais: Integer): string;
  function TamanhoArquivo(Arquivo: string): Integer;


implementation

// Retorna o sobrenome de um nome completo
function SobreNome(Palavra: string): string;
var
  Caracter,Nome: String;
  x,cont: Integer;
begin
  Cont := 0;
  for X := 0 to Length(Trim(Palavra)) do
  begin
     Caracter := Copy(palavra,Length(Trim(Palavra))-x,1);
     cont := cont + 1;
     if Caracter = ' ' then
     begin
        Nome := Copy(palavra,Length(Trim(Palavra))-x,cont);
        if (AnsiUpperCase(Trim(Nome)) = 'FILHO') or (AnsiUpperCase(Trim(Nome)) = 'NETO')or
           (AnsiUpperCase(Trim(Nome)) = 'NETA')  or (AnsiUpperCase(Trim(Nome)) = 'JUNIOR')or
           (AnsiUpperCase(Trim(Nome)) = 'J�NIOR')or (AnsiUpperCase(Trim(Nome)) = 'GENRO') or
           (AnsiUpperCase(Trim(Nome)) = 'SOBRINHO') or (AnsiUpperCase(Nome) = ' ') then
        begin
            Nome := '';
            cont := 0;
        end
        else
        begin
            Break;
        end;
     end;
  end;
  SobreNome := Nome;
end;

// Retorna a string em letra maiusuculas e sem os acentos
function RetiraAcentuacao(Value: string): string;
var
  iValue,
  iAcentos: integer;
  aAcentos: array [1..24] of array [1..2] of char;
begin
  aAcentos[1 ,1] := 'A'; aAcentos[1 ,2] := '�';
  aAcentos[2 ,1] := 'E'; aAcentos[2 ,2] := '�';
  aAcentos[3 ,1] := 'I'; aAcentos[3 ,2] := '�';
  aAcentos[4 ,1] := 'O'; aAcentos[4 ,2] := '�';
  aAcentos[5 ,1] := 'U'; aAcentos[5 ,2] := '�';

  aAcentos[6 ,1] := 'A'; aAcentos[6 ,2] := '�';
  aAcentos[7 ,1] := 'E'; aAcentos[7 ,2] := '�';
  aAcentos[8 ,1] := 'I'; aAcentos[8 ,2] := '�';
  aAcentos[9 ,1] := 'O'; aAcentos[9 ,2] := '�';
  aAcentos[10,1] := 'U'; aAcentos[10,2] := '�';

  aAcentos[11,1] := 'A'; aAcentos[11,2] := '�';
  aAcentos[12,1] := 'E'; aAcentos[12,2] := '�';
  aAcentos[13,1] := 'I'; aAcentos[13,2] := '�';
  aAcentos[14,1] := 'O'; aAcentos[14,2] := '�';
  aAcentos[15,1] := 'U'; aAcentos[15,2] := '�';

  aAcentos[16,1] := 'A'; aAcentos[16,2] := '�';
  aAcentos[17,1] := 'E'; aAcentos[17,2] := '�';
  aAcentos[18,1] := 'I'; aAcentos[18,2] := '�';
  aAcentos[19,1] := 'O'; aAcentos[19,2] := '�';
  aAcentos[20,1] := 'U'; aAcentos[20,2] := '�';

  aAcentos[21,1] := 'A'; aAcentos[21,2] := '�';
  aAcentos[22,1] := 'O'; aAcentos[22,2] := '�';
  aAcentos[23,1] := '�'; aAcentos[23,2] := 'N';
  aAcentos[24,1] := '�'; aAcentos[24,2] := 'C';


  Value := AnsiUpperCase(Value);
  for iValue := 1 to length(Value) do
    for iAcentos := 1 to 24 do
      if Value[iValue] = aAcentos[iAcentos,2] then
         Value[iValue] := aAcentos[iAcentos,1];
  Result := Value;
end;

// Retorna o nome do usu�rio que fez login na esta��o
function NomeUsuario :String ;
var
  pNome :PChar ;
  sNome :String ;
  wTam  :dWord ;
begin

  wTam := 40 ;
  SetLength(sNome, wTam) ;
  pNome := Pchar(sNome) ;
  GetUserName(pNome, wTam) ;
  Result := Trim(String(pNome)) ;

end;

// Retorna o nome do computador
function NomeComputador :String ;
var
  pNome :PChar ;
  sNome :String ;
  wTam  :dWord ;
begin

  wTam := 40 ;
  SetLength(sNome, wTam) ;
  pNome := Pchar(sNome) ;
  GetComputerName(pNome, wTam) ;
  Result := Trim(String(pNome)) ;

end;

// Retorna o IP local de um computador
function IPLocal(NomeDoComputador :String) :String ;
var
  HostEnt: PHostEnt;
  s1, s2, s3, s4 :String ;
begin
  HostEnt := GetHostByName(PChar(NomeComputador));

  if HostEnt <> nil then
  begin
    s1 := IntToStr(Ord(HostEnt^.h_addr^[0])) ;
    s2 := IntToStr(Ord(HostEnt^.h_addr^[1])) ;
    s3 := IntToStr(Ord(HostEnt^.h_addr^[2])) ;
    s4 := IntToStr(Ord(HostEnt^.h_addr^[3])) ;
    Result := s1+'.'+s2+'.'+s3+'.'+s4 ;
  end
  else
    Result:= '<n�o encontrado>'

end ;

{**
  Escreve por extenso um valor fornecido
  Par�metros:
  Valor: string;   Valor em formato string. Ex.: 3521
  MF: string = 'M';  Se o n�mero 1 ser� "UM" ou "UMA".
                                'M'= masculino, 'F' = feminino
}
function Extenso(Valor: string; MF: string = 'M'): string;
var
  vet_Unid: array[1..3, 1..20] of string; {1 = centena, 2 = dezena, 3 = Unidade}
  Ext: string;
  Num: Integer;
begin
  Ext:= '';

  vet_Unid[1, 1]:= 'cento';
  vet_Unid[1, 2]:= 'duzentos';
  vet_Unid[1, 3]:= 'trezentos';
  vet_Unid[1, 4]:= 'quatrocentos';
  vet_Unid[1, 5]:= 'quinhentos';
  vet_Unid[1, 6]:= 'seiscentos';
  vet_Unid[1, 7]:= 'setecentos';
  vet_Unid[1, 8]:= 'oitocentos';
  vet_Unid[1, 9]:= 'novecentos';

  vet_Unid[2, 1]:= 'dez';
  vet_Unid[2, 2]:= 'vinte';
  vet_Unid[2, 3]:= 'trinta';
  vet_Unid[2, 4]:= 'quarenta';
  vet_Unid[2, 5]:= 'cinq�enta';
  vet_Unid[2, 6]:= 'sessenta';
  vet_Unid[2, 7]:= 'setenta';
  vet_Unid[2, 8]:= 'oitenta';
  vet_Unid[2, 9]:= 'noventa';


  //Alterado por Marcos em 22/07/2002
  If UpperCase(MF) = 'M' then
  begin
    vet_Unid[3, 1]:= 'um';
    vet_Unid[3, 2]:= 'dois';
  end
  else
  begin
    vet_Unid[3, 1]:= 'uma';
    vet_Unid[3, 2]:= 'duas';
  end;

  vet_Unid[3, 3]:= 'tr�s';
  vet_Unid[3, 4]:= 'quatro';
  vet_Unid[3, 5]:= 'cinco';
  vet_Unid[3, 6]:= 'seis';
  vet_Unid[3, 7]:= 'sete';
  vet_Unid[3, 8]:= 'oito';
  vet_Unid[3, 9]:= 'nove';
  vet_Unid[3, 10]:= 'dez';
  vet_Unid[3, 11]:= 'onze';
  vet_Unid[3, 12]:= 'doze';
  vet_Unid[3, 13]:= 'treze';
  vet_Unid[3, 14]:= 'quatorze';
  vet_Unid[3, 15]:= 'quinze';
  vet_Unid[3, 16]:= 'dezesseis';
  vet_Unid[3, 17]:= 'dezessete';
  vet_Unid[3, 18]:= 'dezoito';
  vet_Unid[3, 19]:= 'dezenove';

  Valor:= StrZero(Valor, 3, 0);

  if Valor = '100' then
    vet_Unid[1, 1]:= 'cem';

  {---- centena -----}
  Num:= StrToInt(Copy(Valor, 1, 1));
  if Num <> 0 then
    Ext:= Ext + vet_Unid[1, Num];

  {---- dezena -----}
  if Copy(Valor, 2, 2) <> '' then
    Num:= StrToInt(Copy(Valor, 2, 2));

  if Num > 0 then
  begin
    if Num >= 20 then
    begin
      if Ext <> '' then
        Ext:= Ext + ' e ' + vet_Unid[2, StrToInt(Copy(IntToStr(Num), 1, 1))]
      else
        Ext:= Ext + ' ' + vet_Unid[2, StrToInt(Copy(IntToStr(Num), 1, 1))];
            {------ unidade ---------}
      Num:= StrToInt(Copy(Valor, 3, 1));
      if Num > 0 then
        Ext:= Ext + ' e ' + vet_Unid[3, Num];
    end
    else
    begin
      if Ext <> '' then
        Ext:= Ext + ' e ' + vet_Unid[3, Num]
      else
        Ext:= Ext + ' ' + vet_Unid[3, Num];
    end;
  end;
  Extenso:= Ext;
end;

// Retorna o tamanho do arquivo
function TamanhoArquivo(Arquivo: string): Integer;
var
  Arquivo1: file of byte;
begin
  AssignFile(Arquivo1, Arquivo);

  Reset(Arquivo1);
  Result:= Filesize(Arquivo1);
  CloseFile(Arquivo1);
end;

{**
  Retorna uma string contendo o valor passado em 'S' com todas as primeiras letras
  de cada palavra transformadas para mai�scula e as restantes min�sculas.
  Ignora as preposi��es "dos, das, do, da" e a conjun��o aditiva "e".

  Par�metros:
  const S: string;   Conte�do original. Ex.: 'RUA RAMIRO BARCELOS'
  const WordDelims: TCharSet;  Conjunto de caracteres que separam uma palavra da
                                       outra. Ex.: [' ','.','''']

  Exemplo: <font size='1'>
  PrimeiraMaiuscula(ENDREG, [' ','.','''']);
  Logradouro:= PrimeiraMaiuscula(fmConsultaCEP.Logradouro.FieldByName('Nomelogradouro').AsString);
}
function PrimeiraMaiuscula(const S: string; const WordDelims: TCharSet): string;
var
  SLen, I: Cardinal;
begin
  Result:= AnsiLowerCase(S);
  I:= 1;
  SLen:= Length(Result);
  while I <= SLen do
  begin
    while (I <= SLen) and (Result[I] in WordDelims) do
      Inc(I);
    if (I <= SLen) and
      (AnsiLowerCase(Copy(Result, I, 4)) <> 'dos ') and
      (AnsiLowerCase(Copy(Result, I, 4)) <> 'das ') and
      (AnsiLowerCase(Copy(Result, I, 3)) <> 'do ') and
      (AnsiLowerCase(Copy(Result, I, 3)) <> 'da ') and
      (AnsiLowerCase(Copy(Result, I, 3)) <> 'de ') and
      (AnsiLowerCase(Copy(Result, I, 2)) <> 'e ') then
      Result[I]:= AnsiUpperCase(Result[I])[1];

    while (I <= SLen) and not (Result[I] in WordDelims) do
      Inc(I);
  end;
end;

{**A fun��o testa se a express�o passada como par�metro em Expr � verdadeira. Se for,
  retorna o conte�do que estiver em ResultTrue, caso contr�rio o conte�do
  de ResultFalse
  Par�metros:

  Expr: Boolean;  Express�o booleana. Ex: x=0.
  ResultTrue: variant;  Valor retornado em caso da Expr ser verdadeira. Pode ser uma outra
  express�o, ou outro comando Iff.
  ResultFalse: variant;  Valor retornado em caso da Expr ser falsa. Pode ser uma outra
  express�o, ou outro comando Iff.
}
function Iff(Expr: Boolean; ResultTrue: variant; ResultFalse: variant): variant;
begin
  if Expr then
    Result:= ResultTrue
  else
    Result:= ResultFalse;
end;

{**
  Retorna uma string do Valor passado como par�metro preenchida com zeros at� que
  o campo fique do tamanho indicado em TamanhoCampo. Se desejar casas decimais, informar
  a quantidade em QtdeZerosDecimais.
  Par�metros:

  Valor: string;  Valor. Ex.: '500'
  TamanhoCampo: Integer;  Tamanho do campo. Ex.: 7
  QtdeZerosDecimais: Integer;  Quantidade de zeros decimais. Ex.: 2

  Exemplo de retorno: '00500,00'
}
function StrZero(Valor: string; TamanhoCampo: Integer; QtdeZerosDecimais: Integer): string;
var
  TamInteiros, TamDecimais: Integer;
  QtdeZerosInteiros: Integer;
  Inteiros, Decimais: string;
  PosicaoVirgula: Integer;
begin
  Valor:= StringReplace(Valor, '.', '', [rfReplaceAll]);
  PosicaoVirgula:= pos(',', Valor);
  if PosicaoVirgula <> 0 then
  begin
    Inteiros:= Copy(Valor, 1, PosicaoVirgula - 1);
    Decimais:= Copy(Valor, PosicaoVirgula + 1, Length(Valor) - PosicaoVirgula);
  end
  else
  begin
    Inteiros:= Valor;
    Decimais:= '';
  end;

  TamInteiros:= Length(Inteiros);
  TamDecimais:= Length(Decimais);

   {montagem dos inteiros}
  QtdeZerosInteiros:= TamanhoCampo - TamInteiros - QtdeZerosDecimais;
  if (PosicaoVirgula <> 0) or (QtdeZerosDecimais <> 0) then
    Inteiros:= StringOfChar('0', QtdeZerosInteiros - 1) + Inteiros
  else
    Inteiros:= StringOfChar('0', QtdeZerosInteiros) + Inteiros;


   {montagem das decimais}
  if QtdeZerosDecimais > 0 then
  begin
        {QtdeZerosDecimais:= QtdeZerosDecimais - TamDecimais;}

    if TamDecimais > QtdeZerosDecimais then
      Decimais:= Copy(Decimais, 1, QtdeZerosDecimais)
    else
      Decimais:= Decimais + StringOfChar('0', QtdeZerosDecimais - TamDecimais);

    StrZero:= Inteiros + ',' + Decimais;
  end
  else
  begin
    StrZero:= Inteiros;
  end;
end;


end.

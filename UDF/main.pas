unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmMain = class(TForm)
    btIP: TButton;
    btSobrenome: TButton;
    btValorExtenso: TButton;
    btNomeComp: TButton;
    btNomeUsuario: TButton;
    btPriMaiuscula: TButton;
    btRetiraAcento: TButton;
    procedure btIPClick(Sender: TObject);
    procedure btSobrenomeClick(Sender: TObject);
    procedure btValorExtensoClick(Sender: TObject);
    procedure btNomeCompClick(Sender: TObject);
    procedure btNomeUsuarioClick(Sender: TObject);
    procedure btPriMaiusculaClick(Sender: TObject);
    procedure btRetiraAcentoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;

implementation

uses CursoUtils;

{$R *.dfm}

// Buscar o IP de uma esta��o
procedure TfmMain.btIPClick(Sender: TObject);
var
  sAux: string;
begin
  if InputQuery('Nome da esta��o', 'Digite o nome da esta��o', sAux) then
    ShowMessage('IP: ' +  IPLocal(sAux));
end;

procedure TfmMain.btSobrenomeClick(Sender: TObject);
var
  sAux: string;
begin
  if InputQuery('Sobrenome', 'Digite o nome completo', sAux) then
    ShowMessage('Sobrenome: ' +  SobreNome(sAux));
end;

procedure TfmMain.btValorExtensoClick(Sender: TObject);
var
  sAux: string;
begin
  if InputQuery('Extenso', 'Digite o n�mero (sem v�rgulas)', sAux) then
    ShowMessage('N�mero extenso: ' +  Extenso(sAux));
end;

procedure TfmMain.btNomeCompClick(Sender: TObject);
begin
  ShowMessage(NomeComputador);
end;

procedure TfmMain.btNomeUsuarioClick(Sender: TObject);
begin
  ShowMessage(NomeUsuario);
end;

procedure TfmMain.btPriMaiusculaClick(Sender: TObject);
var
  sAux: string;
begin
  if InputQuery('Primeira mai�scula', 'Digite o nome completo', sAux) then
    ShowMessage('Primeira mai�scula: ' +  PrimeiraMaiuscula(sAux, [' ', ',', '.']));
end;

procedure TfmMain.btRetiraAcentoClick(Sender: TObject);
var
  sAux: string;
begin
  if InputQuery('Retira acentos', 'Digite palavras acentuadas', sAux) then
    ShowMessage('Palavra sem acentos: ' +  RetiraAcentuacao(sAux));
end;

end.

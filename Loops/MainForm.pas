unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TfmMain = class(TForm)
    Cima: TButton;
    Baixo: TButton;
    Direita: TButton;
    Esquerda: TButton;
    Parar: TButton;
    Bola: TImage;
    procedure DireitaClick(Sender: TObject);
    procedure EsquerdaClick(Sender: TObject);
    procedure BaixoClick(Sender: TObject);
    procedure CimaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;

const
  TempoEspera = 5;

implementation

{$R *.dfm}

procedure TfmMain.DireitaClick(Sender: TObject);
var
  i, inicio, limite: integer;
begin
  inicio:= Bola.Left; //--> pega a posi��o atual da bola
  limite:= fmMain.Width - Bola.Width - inicio;
  for i:= inicio to limite do //--> anda da posi��o atual at� o limite da tela
  begin
    Bola.Left:= i;              //--> atribui o novo valor da posi��o
    Bola.Update;                //--> atualiza a imagem na nova posi��o
    Sleep(TempoEspera);         //--> espera um tempo para continuar
  end;
end;

procedure TfmMain.EsquerdaClick(Sender: TObject);
var
  i, inicio, limite: integer;
begin
  inicio:= Bola.Left; //--> pega a posi��o atual da bola
  limite:= 0;
  for i:= inicio downto limite do //--> anda da posi��o atual at� o limite da tela
  begin
    Bola.Left:= i;              //--> atribui o novo valor da posi��o
    Bola.Update;                //--> atualiza a imagem na nova posi��o
    Sleep(TempoEspera);         //--> espera um tempo para continuar
  end;

end;

procedure TfmMain.BaixoClick(Sender: TObject);
var
  limite: integer;
begin
  limite:= fmMain.Height - Bola.Height - Bola.Top;

  while Bola.Top <= limite do   //--> anda da posi��o atual at� o limite da tela
  begin
    Bola.Top:= Bola.Top + 10;   //--> atribui o novo valor da posi��o
    Bola.Update;                //--> atualiza a imagem na nova posi��o
    Sleep(TempoEspera);         //--> espera um tempo para continuar
  end;

end;

procedure TfmMain.CimaClick(Sender: TObject);
var
  limite: integer;
begin
  limite:= 0;

  repeat //--> repete todos os comandos abaixo
    Bola.Top:= Bola.Top - 10;   //--> atribui o novo valor da posi��o
    Bola.Update;                //--> atualiza a imagem na nova posi��o
    Sleep(TempoEspera);         //--> espera um tempo para continuar
  until Bola.Top <= limite;     //--> at� que a posi��o chegue ao limite
end;

end.

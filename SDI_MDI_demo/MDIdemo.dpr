program MDIdemo;

uses
  Forms,
  MDIForm in 'MDIForm.pas' {fmMDI},
  MDIManutencaoForm in 'MDIManutencaoForm.pas' {fmMDIManutencao},
  MDIImprimirForm in 'MDIImprimirForm.pas' {fmMDIImprimir};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMDI, fmMDI);
  Application.Run;
end.

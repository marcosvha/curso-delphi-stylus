object fmMDIImprimir: TfmMDIImprimir
  Left = 236
  Top = 114
  BorderStyle = bsDialog
  Caption = 'fmMDIImprimir'
  ClientHeight = 446
  ClientWidth = 301
  Color = clBtnFace
  Constraints.MaxHeight = 480
  Constraints.MaxWidth = 309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 281
    Height = 377
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Imprimir: TButton
    Left = 128
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Imprimir'
    TabOrder = 1
  end
  object Cancelar: TButton
    Left = 216
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 2
  end
end

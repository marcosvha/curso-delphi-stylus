program SDIdemo;

uses
  Forms,
  SDIForm in 'SDIForm.pas' {fmSDI},
  SDIManutencaoForm in 'SDIManutencaoForm.pas' {fmSDIManutencao},
  SDIImprimirForm in 'SDIImprimirForm.pas' {fmSDIImprimir};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmSDI, fmSDI);
  Application.CreateForm(TfmSDIManutencao, fmSDIManutencao);
  Application.CreateForm(TfmSDIImprimir, fmSDIImprimir);
  Application.Run;
end.

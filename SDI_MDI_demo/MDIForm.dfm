object fmMDI: TfmMDI
  Left = 259
  Top = 168
  Width = 696
  Height = 480
  Caption = 'fmMDI'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 88
    Top = 80
    object Estoque1: TMenuItem
      Caption = 'Estoque'
      object Manuteno1: TMenuItem
        Caption = 'Manuten'#231#227'o'
        OnClick = Manuteno1Click
      end
      object Relatrios1: TMenuItem
        Caption = 'Relat'#243'rios'
        OnClick = Relatrios1Click
      end
    end
    object Faturamento1: TMenuItem
      Caption = 'Faturamento'
      object Manuteno2: TMenuItem
        Caption = 'Manuten'#231#227'o'
        OnClick = Manuteno2Click
      end
      object Relatrios2: TMenuItem
        Caption = 'Relat'#243'rios'
        OnClick = Relatrios2Click
      end
    end
    object Caixa1: TMenuItem
      Caption = 'Caixa'
      object Manuteno3: TMenuItem
        Caption = 'Manuten'#231#227'o'
        OnClick = Manuteno3Click
      end
      object Relatrios3: TMenuItem
        Caption = 'Relat'#243'rios'
        OnClick = Relatrios3Click
      end
    end
    object abelas1: TMenuItem
      Caption = 'Tabelas'
      object Clientes1: TMenuItem
        Caption = 'Clientes'
        OnClick = Clientes1Click
      end
      object Fornecedores1: TMenuItem
        Caption = 'Fornecedores'
        OnClick = Fornecedores1Click
      end
      object Produtos1: TMenuItem
        Caption = 'Produtos'
        OnClick = Produtos1Click
      end
    end
    object Ajuda1: TMenuItem
      Caption = 'Ajuda'
      object Contedo1: TMenuItem
        Caption = 'Conte'#250'do...'
      end
      object Sobre1: TMenuItem
        Caption = 'Sobre'
        OnClick = Sobre1Click
      end
    end
  end
end

unit MDIForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus;

type
  TfmMDI = class(TForm)
    MainMenu1: TMainMenu;
    Estoque1: TMenuItem;
    Manuteno1: TMenuItem;
    Relatrios1: TMenuItem;
    Faturamento1: TMenuItem;
    Manuteno2: TMenuItem;
    Relatrios2: TMenuItem;
    Caixa1: TMenuItem;
    Manuteno3: TMenuItem;
    Relatrios3: TMenuItem;
    abelas1: TMenuItem;
    Clientes1: TMenuItem;
    Fornecedores1: TMenuItem;
    Produtos1: TMenuItem;
    Ajuda1: TMenuItem;
    Contedo1: TMenuItem;
    Sobre1: TMenuItem;
    procedure Manuteno1Click(Sender: TObject);
    procedure Manuteno2Click(Sender: TObject);
    procedure Manuteno3Click(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure Fornecedores1Click(Sender: TObject);
    procedure Produtos1Click(Sender: TObject);
    procedure Sobre1Click(Sender: TObject);
    procedure Relatrios1Click(Sender: TObject);
    procedure Relatrios2Click(Sender: TObject);
    procedure Relatrios3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMDI: TfmMDI;

implementation

uses MDIImprimirForm, MDIManutencaoForm;

{$R *.dfm}

procedure TfmMDI.Manuteno1Click(Sender: TObject);
var
  f: TfmMDIManutencao;
begin
  f:= TfmMDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Estoque';
  f.Show;
end;

procedure TfmMDI.Manuteno2Click(Sender: TObject);
var
  f: TfmMDIManutencao;
begin
  f:= TfmMDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Faturamento';
  f.Show;

end;

procedure TfmMDI.Manuteno3Click(Sender: TObject);
var
  f: TfmMDIManutencao;
begin
  f:= TfmMDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Caixa';
  f.Show;

end;

procedure TfmMDI.Clientes1Click(Sender: TObject);
var
  f: TfmMDIManutencao;
begin
  f:= TfmMDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Clientes';
  f.Show;

end;

procedure TfmMDI.Fornecedores1Click(Sender: TObject);
var
  f: TfmMDIManutencao;
begin
  f:= TfmMDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Fornecedores';
  f.Show;

end;

procedure TfmMDI.Produtos1Click(Sender: TObject);
var
  f: TfmMDIManutencao;
begin
  f:= TfmMDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Produtos';
  f.Show;

end;

procedure TfmMDI.Sobre1Click(Sender: TObject);
begin
  ShowMessage('Demo de aplicação MDI' + #13 + #13 + 'por Marcos Henke');
end;

procedure TfmMDI.Relatrios1Click(Sender: TObject);
var
  f: TfmMDIImprimir;
begin
  f:= TfmMDIImprimir.Create(self);
  f.Caption:= 'Impressão de Estoque';
  f.Show;

end;

procedure TfmMDI.Relatrios2Click(Sender: TObject);
var
  f: TfmMDIImprimir;
begin
  f:= TfmMDIImprimir.Create(self);
  f.Caption:= 'Impressão de Faturamento';
  f.Show;

end;

procedure TfmMDI.Relatrios3Click(Sender: TObject);
var
  f: TfmMDIImprimir;
begin
  f:= TfmMDIImprimir.Create(self);
  f.Caption:= 'Impressão de Caixa';
  f.Show;

end;

end.

object fmMDIManutencao: TfmMDIManutencao
  Left = 188
  Top = 183
  Width = 696
  Height = 480
  Caption = 'fmMDIManutencao'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 16
    Top = 16
    Width = 657
    Height = 353
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Incluir: TButton
    Left = 40
    Top = 384
    Width = 75
    Height = 25
    Caption = 'Incluir'
    TabOrder = 1
  end
  object Alterar: TButton
    Left = 128
    Top = 384
    Width = 75
    Height = 25
    Caption = 'Alterar'
    TabOrder = 2
  end
  object Excluir: TButton
    Left = 216
    Top = 384
    Width = 75
    Height = 25
    Caption = 'Excluir'
    TabOrder = 3
  end
end

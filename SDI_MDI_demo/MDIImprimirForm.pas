unit MDIImprimirForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids;

type
  TfmMDIImprimir = class(TForm)
    DBGrid1: TDBGrid;
    Imprimir: TButton;
    Cancelar: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMDIImprimir: TfmMDIImprimir;

implementation

{$R *.dfm}

procedure TfmMDIImprimir.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:= caFree;
end;

end.

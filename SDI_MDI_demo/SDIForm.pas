unit SDIForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus;

type
  TfmSDI = class(TForm)
    MainMenu1: TMainMenu;
    Estoque1: TMenuItem;
    Faturamento1: TMenuItem;
    Caixa1: TMenuItem;
    abelas1: TMenuItem;
    Ajuda1: TMenuItem;
    Manuteno1: TMenuItem;
    Relatrios1: TMenuItem;
    Manuteno2: TMenuItem;
    Relatrios2: TMenuItem;
    Manuteno3: TMenuItem;
    Relatrios3: TMenuItem;
    Clientes1: TMenuItem;
    Fornecedores1: TMenuItem;
    Produtos1: TMenuItem;
    Contedo1: TMenuItem;
    Sobre1: TMenuItem;
    procedure Manuteno1Click(Sender: TObject);
    procedure Manuteno2Click(Sender: TObject);
    procedure Manuteno3Click(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure Fornecedores1Click(Sender: TObject);
    procedure Produtos1Click(Sender: TObject);
    procedure Sobre1Click(Sender: TObject);
    procedure Relatrios1Click(Sender: TObject);
    procedure Relatrios2Click(Sender: TObject);
    procedure Relatrios3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSDI: TfmSDI;

implementation

uses SDIImprimirForm, SDIManutencaoForm;

{$R *.dfm}

procedure TfmSDI.Manuteno1Click(Sender: TObject);
var
  f: TfmSDIManutencao;
begin
  f:= TfmSDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Estoque';
  f.ShowModal;
  f.Free;
end;

procedure TfmSDI.Manuteno2Click(Sender: TObject);
var
  f: TfmSDIManutencao;
begin
  f:= TfmSDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Faturamento';
  f.ShowModal;
  f.Free;

end;

procedure TfmSDI.Manuteno3Click(Sender: TObject);
var
  f: TfmSDIManutencao;
begin
  f:= TfmSDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Caixa';
  f.ShowModal;
  f.Free;

end;

procedure TfmSDI.Clientes1Click(Sender: TObject);
var
  f: TfmSDIManutencao;
begin
  f:= TfmSDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Clientes';
  f.ShowModal;
  f.Free;

end;

procedure TfmSDI.Fornecedores1Click(Sender: TObject);
var
  f: TfmSDIManutencao;
begin
  f:= TfmSDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Fornecedores';
  f.ShowModal;
  f.Free;

end;

procedure TfmSDI.Produtos1Click(Sender: TObject);
var
  f: TfmSDIManutencao;
begin
  f:= TfmSDIManutencao.Create(self);
  f.Caption:= 'Manutenção de Produtos';
  f.ShowModal;
  f.Free;

end;

procedure TfmSDI.Sobre1Click(Sender: TObject);
begin
  ShowMessage('Demo de aplicação SDI' + #13 + #13 +'por Marcos Henke');
end;

procedure TfmSDI.Relatrios1Click(Sender: TObject);
var
  f: TfmSDIImprimir;
begin
  f:= TfmSDIImprimir.Create(self);
  f.Caption:= 'Impressão de Estoque';
  f.ShowModal;
  f.Free;

end;

procedure TfmSDI.Relatrios2Click(Sender: TObject);
var
  f: TfmSDIImprimir;
begin
  f:= TfmSDIImprimir.Create(self);
  f.Caption:= 'Impressão de Faturamento';
  f.ShowModal;
  f.Free;
end;

procedure TfmSDI.Relatrios3Click(Sender: TObject);
var
  f: TfmSDIImprimir;
begin
  f:= TfmSDIImprimir.Create(self);
  f.Caption:= 'Impressão de Caixa';
  f.ShowModal;
  f.Free;
end;

end.

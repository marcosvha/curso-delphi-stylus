unit MDIManutencaoForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids;

type
  TfmMDIManutencao = class(TForm)
    DBGrid1: TDBGrid;
    Incluir: TButton;
    Alterar: TButton;
    Excluir: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMDIManutencao: TfmMDIManutencao;

implementation

{$R *.dfm}

procedure TfmMDIManutencao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:= caFree;
end;

end.

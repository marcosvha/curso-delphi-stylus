object Form1: TForm1
  Left = 165
  Top = 123
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    688
    446)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 49
    Height = 13
    Caption = 'Database:'
  end
  object edSQL: TMemo
    Left = 16
    Top = 64
    Width = 545
    Height = 185
    Anchors = [akLeft, akTop, akRight]
    Lines.Strings = (
      'select * from clientes')
    TabOrder = 0
  end
  object edDatabase: TEdit
    Left = 16
    Top = 32
    Width = 209
    Height = 21
    TabOrder = 1
    Text = 'agenda'
  end
  object btConecta: TButton
    Left = 240
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Conectar'
    TabOrder = 2
    OnClick = btConectaClick
  end
  object DBGrid: TDBGrid
    Left = 16
    Top = 264
    Width = 513
    Height = 169
    DataSource = DataSource
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object btExecutar: TButton
    Left = 568
    Top = 64
    Width = 113
    Height = 25
    Caption = 'Executar SQL'
    TabOrder = 4
    OnClick = btExecutarClick
  end
  object Button1: TButton
    Left = 568
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 5
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 608
    Top = 152
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 6
    OnClick = Button2Click
  end
  object ListBox: TListBox
    Left = 560
    Top = 256
    Width = 121
    Height = 169
    ItemHeight = 13
    TabOrder = 7
    OnClick = ListBoxClick
  end
  object DataSource: TDataSource
    DataSet = ZQuery
    Left = 568
    Top = 112
  end
  object Query: TQuery
    SQL.Strings = (
      '')
    Left = 608
    Top = 112
  end
  object Teste: TSimpleDataSet
    Aggregates = <>
    AutoCalcFields = False
    Connection.ConnectionName = 'MySQLConnection'
    Connection.DriverName = 'MySQL'
    Connection.GetDriverFunc = 'getSQLDriverMYSQL'
    Connection.LibraryName = 'dbexpmys.dll'
    Connection.LoginPrompt = False
    Connection.Params.Strings = (
      'DriverName=MySQL'
      'HostName=localhost'
      'Database=clientes'
      'User_Name=root'
      'Password=marcos'
      'BlobSize=-1'
      'LocaleCode=0000')
    Connection.VendorLib = 'LIBMYSQL.dll'
    Connection.Connected = True
    DataSet.NoMetadata = True
    DataSet.ObjectView = True
    DataSet.MaxBlobSize = -1
    DataSet.ParamCheck = False
    DataSet.Params = <>
    PacketRecords = 100
    Params = <>
    Left = 568
    Top = 152
  end
  object ZConnection: TZConnection
    Protocol = 'mysql-3.23'
    HostName = 'localhost'
    Port = 0
    Database = 'clientes'
    User = 'root'
    Password = 'marcos'
    AutoCommit = True
    ReadOnly = True
    TransactIsolationLevel = tiNone
    Connected = True
    Left = 416
    Top = 16
  end
  object ZQuery: TZQuery
    Connection = ZConnection
    RequestLive = False
    CachedUpdates = False
    SQL.Strings = (
      'select * from clientes')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Left = 448
    Top = 16
  end
  object RvDataSetConnection1: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = ZQuery
    Left = 568
    Top = 208
  end
  object RvProject: TRvProject
    LoadDesigner = True
    ProjectFile = 'relatorios.rav'
    Left = 608
    Top = 208
  end
  object RvRenderPDF1: TRvRenderPDF
    DisplayName = 'Adobe Acrobat (PDF)'
    FileExtension = '*.pdf'
    EmbedFonts = False
    ImageQuality = 90
    MetafileDPI = 300
    FontEncoding = feWinAnsiEncoding
    DocInfo.Creator = 'Rave (http://www.nevrona.com/rave)'
    DocInfo.Producer = 'Nevrona Designs'
    Left = 344
    Top = 16
  end
  object RvRenderHTML1: TRvRenderHTML
    DisplayName = 'Web Page (HTML)'
    FileExtension = '*.html;*.htm'
    ServerMode = False
    Left = 344
    Top = 56
  end
  object RvRenderRTF1: TRvRenderRTF
    DisplayName = 'Rich Text Format (RTF)'
    FileExtension = '*.rtf'
    Left = 352
    Top = 104
  end
  object RvRenderText1: TRvRenderText
    DisplayName = 'Plain Text (TXT)'
    FileExtension = '*.txt'
    CPI = 10.000000000000000000
    LPI = 6.000000000000000000
    Left = 352
    Top = 136
  end
end

unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, DB, DBTables, DBXpress, FMTBcd,
  SqlExpr, DBClient, SimpleDS, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, ZConnection, RpRave, RpDefine, RpCon, RpConDS, RpRender,
  RpRenderPDF, RpRenderText, RpRenderRTF, RpRenderHTML;

type
  TForm1 = class(TForm)
    edSQL: TMemo;
    edDatabase: TEdit;
    Label1: TLabel;
    btConecta: TButton;
    DBGrid: TDBGrid;
    btExecutar: TButton;
    DataSource: TDataSource;
    Query: TQuery;
    Button1: TButton;
    Teste: TSimpleDataSet;
    ZConnection: TZConnection;
    ZQuery: TZQuery;
    RvDataSetConnection1: TRvDataSetConnection;
    RvProject: TRvProject;
    Button2: TButton;
    ListBox: TListBox;
    RvRenderPDF1: TRvRenderPDF;
    RvRenderHTML1: TRvRenderHTML;
    RvRenderRTF1: TRvRenderRTF;
    RvRenderText1: TRvRenderText;
    procedure btConectaClick(Sender: TObject);
    procedure btExecutarClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btConectaClick(Sender: TObject);
begin
  Query.Close;
  Query.DatabaseName:= edDatabase.Text;
  
end;

procedure TForm1.btExecutarClick(Sender: TObject);
begin
//  Query.Close;
//  Query.SQL.Text:= edSQL.Lines.Text;
//  Query.Open;

  ZQuery.Close;
  ZQuery.SQL.Text:= edSQL.Lines.Text;
  ZQuery.Open;



end;

procedure TForm1.Button1Click(Sender: TObject);
//var
//  slAux: TStringList;
//  StringData: String;
begin
//  slAux:= TStringList.Create;
//
//  while not query.eof do
//  begin
//    slAux.Add(format('insert into cidades (cidade, uf) '+
//           'values ( %s, %s);', [
//           '"' + query.fields[1].AsString + '"',
//           '"' + query.fields[2].AsString + '"']));
//
//
//(*    if query.fields[5].AsString <> '' then
//    StringData:=
//           Copy(query.fields[5].AsString,length(query.fields[5].AsString)-3, 4){ano} + '-' +
//           Copy(query.fields[5].AsString,pos('/', query.fields[5].AsString) + 1, 1){mes} + '-' +
//           Copy(query.fields[5].AsString,1,pos('/', query.fields[5].AsString)-1){dia}
//    else stringdata:= '';
//
//
//
//    slAux.Add(format('insert into clientes (nome, endereco, bairro, cep, sexo, cidcod, dtnasc) '+
//           'values ( %s, %s, %s , %s ,%s ,%s, %s);', [
//           '"' + query.fields[1].AsString + '"',
//           '"' + query.fields[2].AsString + '"',
//           '"' + query.fields[3].AsString + '"',
//           '"' + query.fields[4].AsString + '"',
//           '"' + Copy(query.fields[6].AsString, 1, 1) + '"',
//           query.fields[7].AsString,
//           '"' + stringData + '"']));   *)
//    Query.Next;
//  end;
//
//  slAux.SaveToFile('c:\chuncho.txt');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  rvProject.Execute;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  RvProject.Open; //--> abre o arquivo de projeto que contém os relatórios criados pelo Rave
  RvProject.GetReportList(ListBox.Items,true); //--> carrega a lista de relatórios do projeto para um ListBox
end;

procedure TForm1.ListBoxClick(Sender: TObject);
begin
  RvProject.SelectReport(ListBox.Items[ListBox.ItemIndex],true); //--> seleciona o relatório ao clicar no LB
end;

end.

unit StylusLabel;

interface

uses
  SysUtils, Classes, Controls, StdCtrls;

type
  TStylusLabel = class(TLabel)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); Override;
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Stylus', [TStylusLabel]);
end;

constructor TStylusLabel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Font.Color:= $00984E00;
  Font.Name := 'Comic Sans MS';
  Font.Size := 14;
end;


end.

unit StylusButton;

interface

uses
  SysUtils, Classes, Controls, StdCtrls, Buttons;

type
  TStylusButton = class(TBitBtn)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Stylus', [TStylusButton]);
end;

procedure TStylusButton.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  if (X < Width -5) and
     (Y < Height -5)and
     (X > 5) and
     (Y > 5) then
    Font.Color:= $00984E00
  else
    Font.Color:= $00000000;
end;

end.

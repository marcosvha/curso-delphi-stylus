object Form1: TForm1
  Left = 307
  Top = 114
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 112
    Top = 136
    Width = 32
    Height = 15
    Caption = 'Label1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 9981440
    Font.Height = -11
    Font.Name = 'Comic Sans MS'
    Font.Style = []
    ParentFont = False
  end
  object StylusLabel1: TStylusLabel
    Left = 312
    Top = 176
    Width = 110
    Height = 26
    Caption = 'StylusLabel1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 9981440
    Font.Height = -19
    Font.Name = 'Comic Sans MS'
    Font.Style = []
    ParentFont = False
  end
  object StylusLabel2: TStylusLabel
    Left = 152
    Top = 240
    Width = 113
    Height = 26
    Caption = 'StylusLabel2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 9981440
    Font.Height = -19
    Font.Name = 'Comic Sans MS'
    Font.Style = []
    ParentFont = False
  end
  object StylusLabel3: TStylusLabel
    Left = 248
    Top = 288
    Width = 113
    Height = 26
    Caption = 'StylusLabel3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 9981440
    Font.Height = -19
    Font.Name = 'Comic Sans MS'
    Font.Style = []
    ParentFont = False
  end
  object StylusLabel4: TStylusLabel
    Left = 392
    Top = 280
    Width = 113
    Height = 26
    Caption = 'StylusLabel4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 9981440
    Font.Height = -19
    Font.Name = 'Comic Sans MS'
    Font.Style = []
    ParentFont = False
  end
  object Button1: TButton
    Left = 280
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnEnter = Button1Enter
    OnExit = Button1Exit
  end
end

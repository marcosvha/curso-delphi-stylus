object fmMemo: TfmMemo
  Left = 25
  Top = 142
  Width = 730
  Height = 372
  BorderStyle = bsSizeToolWin
  Caption = 'Memo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    722
    343)
  PixelsPerInch = 96
  TextHeight = 13
  object edTexto: TEdit
    Left = 16
    Top = 16
    Width = 321
    Height = 21
    TabOrder = 0
  end
  object Memo: TMemo
    Left = 16
    Top = 112
    Width = 691
    Height = 221
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
  end
  object btAdicionar: TButton
    Left = 16
    Top = 48
    Width = 113
    Height = 25
    Caption = 'Adicionar ao lado'
    TabOrder = 2
    OnClick = btAdicionarClick
  end
  object btLimpar: TButton
    Left = 144
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 3
    OnClick = btLimparClick
  end
  object btAdicionarLinha: TButton
    Left = 232
    Top = 48
    Width = 105
    Height = 25
    Caption = 'Adicionar abaixo'
    TabOrder = 4
    OnClick = btAdicionarLinhaClick
  end
  object GroupBox1: TGroupBox
    Left = 368
    Top = 16
    Width = 297
    Height = 57
    Caption = 'Alinhamento'
    TabOrder = 5
    object rbEsquerda: TRadioButton
      Left = 8
      Top = 24
      Width = 81
      Height = 17
      Caption = 'Esquerda'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbEsquerdaClick
    end
    object rbDireita: TRadioButton
      Left = 216
      Top = 24
      Width = 63
      Height = 17
      Caption = 'Direita'
      TabOrder = 1
      OnClick = rbDireitaClick
    end
    object rbCentro: TRadioButton
      Left = 120
      Top = 24
      Width = 63
      Height = 17
      Caption = 'Centro'
      TabOrder = 2
      OnClick = rbCentroClick
    end
  end
  object cbSomenteLeitura: TCheckBox
    Left = 16
    Top = 80
    Width = 97
    Height = 17
    Caption = 'Somente leitura'
    TabOrder = 6
    OnClick = cbSomenteLeituraClick
  end
end

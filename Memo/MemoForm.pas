unit MemoForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmMemo = class(TForm)
    edTexto: TEdit;
    Memo: TMemo;
    btAdicionar: TButton;
    btLimpar: TButton;
    btAdicionarLinha: TButton;
    GroupBox1: TGroupBox;
    rbEsquerda: TRadioButton;
    rbDireita: TRadioButton;
    rbCentro: TRadioButton;
    cbSomenteLeitura: TCheckBox;
    procedure btAdicionarClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
    procedure btAdicionarLinhaClick(Sender: TObject);
    procedure cbSomenteLeituraClick(Sender: TObject);
    procedure rbEsquerdaClick(Sender: TObject);
    procedure rbCentroClick(Sender: TObject);
    procedure rbDireitaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMemo: TfmMemo;

implementation

{$R *.dfm}

procedure TfmMemo.btAdicionarClick(Sender: TObject);
begin
  Memo.Text:= Trim(Memo.Text + ' ' + edTexto.Text); //Trim: Elimina espa�os do lado
                                                    //esquerdo e direito de uma String    
end;

procedure TfmMemo.btLimparClick(Sender: TObject);
begin
  Memo.Clear;
end;

procedure TfmMemo.btAdicionarLinhaClick(Sender: TObject);
begin
  Memo.Lines.Add(edTexto.Text);
end;

procedure TfmMemo.cbSomenteLeituraClick(Sender: TObject);
begin
  Memo.ReadOnly:= cbSomenteLeitura.Checked;
end;

procedure TfmMemo.rbEsquerdaClick(Sender: TObject);
begin
  Memo.Alignment:= taLeftJustify;
end;

procedure TfmMemo.rbCentroClick(Sender: TObject);
begin
  Memo.Alignment:= taCenter;
end;

procedure TfmMemo.rbDireitaClick(Sender: TObject);
begin
  Memo.Alignment:= taRightJustify;
end;

end.

unit SinaleiraForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmSinaleira = class(TForm)
    Sinal: TLabel;
    btVerde: TButton;
    btAmarelo: TButton;
    btVermelho: TButton;
    GroupBox: TGroupBox;
    rbVerde: TRadioButton;
    rbAmarelo: TRadioButton;
    rbVermelho: TRadioButton;
    gbAlinhamento: TGroupBox;
    rbEsquerda: TRadioButton;
    rbCentro: TRadioButton;
    rbDireita: TRadioButton;
    procedure btVerdeClick(Sender: TObject);
    procedure btAmareloClick(Sender: TObject);
    procedure btVermelhoClick(Sender: TObject);
    procedure rbVerdeClick(Sender: TObject);
    procedure rbAmareloClick(Sender: TObject);
    procedure rbVermelhoClick(Sender: TObject);
    procedure rbEsquerdaClick(Sender: TObject);
    procedure rbCentroClick(Sender: TObject);
    procedure rbDireitaClick(Sender: TObject);
    procedure btVerdeMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btAmareloMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btVermelhoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSinaleira: TfmSinaleira;

implementation

{$R *.dfm}

procedure TfmSinaleira.btVerdeClick(Sender: TObject);
begin
  Sinal.Font.Color:= clGreen;
  Sinal.Caption:= btVerde.Caption;
  rbVerde.Checked:= True;
end;

procedure TfmSinaleira.btAmareloClick(Sender: TObject);
begin
  Sinal.Font.Color:= clYellow;
  Sinal.Caption:= btAmarelo.Caption;
  rbAmarelo.Checked:= True;
end;

procedure TfmSinaleira.btVermelhoClick(Sender: TObject);
begin
  Sinal.Font.Color:= clRed;
//  Sinal.Caption:= 'Vermelho';
  Sinal.Caption:= btVermelho.Caption;
  rbVermelho.Checked:= True;  
end;

procedure TfmSinaleira.rbVerdeClick(Sender: TObject);
begin
  Sinal.Font.Color:= clGreen;
  Sinal.Caption:= btVerde.Caption;

end;

procedure TfmSinaleira.rbAmareloClick(Sender: TObject);
begin
  Sinal.Font.Color:= clYellow;
  Sinal.Caption:= btAmarelo.Caption;

end;

procedure TfmSinaleira.rbVermelhoClick(Sender: TObject);
begin
  Sinal.Font.Color:= clRed;
  Sinal.Caption:= btVermelho.Caption;
end;

procedure TfmSinaleira.rbEsquerdaClick(Sender: TObject);
begin
  Sinal.Alignment:= taLeftJustify;
end;

procedure TfmSinaleira.rbCentroClick(Sender: TObject);
begin
  Sinal.Alignment:= taCenter;
end;

procedure TfmSinaleira.rbDireitaClick(Sender: TObject);
begin
  Sinal.Alignment:= taRightJustify;
end;

procedure TfmSinaleira.btVerdeMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  Sinal.Font.Color:= clGreen;
  Sinal.Caption:= btVerde.Caption;
end;

procedure TfmSinaleira.btAmareloMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  Sinal.Font.Color:= clYellow;
  Sinal.Caption:= btAmarelo.Caption;
end;

procedure TfmSinaleira.btVermelhoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  Sinal.Font.Color:= clRed;
//  Sinal.Caption:= 'Vermelho';
  Sinal.Caption:= btVermelho.Caption;
end;

end.

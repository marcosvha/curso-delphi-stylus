object fmListas: TfmListas
  Left = -1
  Top = 100
  Width = 796
  Height = 434
  BorderStyle = bsSizeToolWin
  Caption = 'Listas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object edTexto: TEdit
    Left = 16
    Top = 16
    Width = 321
    Height = 21
    TabOrder = 0
  end
  object btLimpar: TButton
    Left = 144
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 1
    OnClick = btLimparClick
  end
  object btAdicionar: TButton
    Left = 16
    Top = 48
    Width = 105
    Height = 25
    Caption = 'Adicionar'
    TabOrder = 2
    OnClick = btAdicionarLinhaClick
  end
  object ComboBox: TComboBox
    Left = 16
    Top = 136
    Width = 169
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
  end
  object ListBox: TListBox
    Left = 200
    Top = 136
    Width = 185
    Height = 249
    ItemHeight = 13
    TabOrder = 4
  end
  object RadioGroup: TRadioGroup
    Left = 408
    Top = 136
    Width = 169
    Height = 249
    Caption = 'RadioGroup'
    TabOrder = 5
  end
  object CheckListBox: TCheckListBox
    Left = 624
    Top = 136
    Width = 153
    Height = 249
    ItemHeight = 13
    TabOrder = 6
  end
end

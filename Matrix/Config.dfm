object fmConfig: TfmConfig
  Left = 380
  Top = 186
  BorderStyle = bsDialog
  Caption = 'Configura'#231#245'es da Matrix'
  ClientHeight = 339
  ClientWidth = 302
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 59
    Width = 64
    Height = 13
    Caption = 'Cor de fundo:'
  end
  object Label2: TLabel
    Left = 18
    Top = 96
    Width = 56
    Height = 13
    Caption = 'Velocidade:'
  end
  object Fonte: TBitBtn
    Left = 16
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Fonte'
    TabOrder = 0
    OnClick = FonteClick
  end
  object Matrix2: TStringGrid
    Left = 16
    Top = 153
    Width = 273
    Height = 145
    BorderStyle = bsNone
    Color = clBlack
    ColCount = 50
    DefaultColWidth = 24
    FixedCols = 0
    RowCount = 50
    FixedRows = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goRangeSelect]
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 1
  end
  object btOK: TButton
    Left = 16
    Top = 305
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = btAplicarClick
  end
  object btCancelar: TButton
    Left = 112
    Top = 305
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btCancelarClick
  end
  object btAplicar: TButton
    Left = 208
    Top = 305
    Width = 75
    Height = 25
    Caption = 'Aplicar'
    TabOrder = 4
    OnClick = btAplicarClick
  end
  object ColorBox1: TColorBox
    Left = 96
    Top = 56
    Width = 145
    Height = 22
    ItemHeight = 16
    TabOrder = 5
    OnChange = ColorBox1Change
  end
  object ScrollBar1: TScrollBar
    Left = 96
    Top = 96
    Width = 145
    Height = 17
    PageSize = 0
    Position = 100
    TabOrder = 6
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Left = 96
    Top = 16
  end
end

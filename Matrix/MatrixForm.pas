unit MatrixForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ExtCtrls;

type
  TfmMatrix = class(TForm)
    Matrix1: TStringGrid;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure Matrix1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PreencheMatrix(Matrix: TStringGrid);
  end;

var
  fmMatrix: TfmMatrix;

implementation

uses Config;

{$R *.dfm}

procedure TfmMatrix.Timer1Timer(Sender: TObject);
begin
  PreencheMatrix(Matrix1);
end;

procedure TfmMatrix.Matrix1DblClick(Sender: TObject);
begin
  fmConfig:= TfmConfig.Create(Self);
  fmConfig.ShowModal;
  fmConfig.Free;
end;

procedure TfmMatrix.PreencheMatrix(Matrix: TStringGrid);
var
  Lin, Col: Integer;
begin
  For Lin:= 0 to 50 do
    For Col:= 0 to 50 do
      Matrix.Cells[Col, Lin]:= Chr(50+Random(50));
end;

end.

unit Config;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, ExtCtrls;

type
  TfmConfig = class(TForm)
    FontDialog1: TFontDialog;
    Fonte: TBitBtn;
    Matrix2: TStringGrid;
    btOK: TButton;
    btCancelar: TButton;
    btAplicar: TButton;
    ColorBox1: TColorBox;
    Label1: TLabel;
    ScrollBar1: TScrollBar;
    Label2: TLabel;
    procedure FonteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure ColorBox1Change(Sender: TObject);
    procedure btAplicarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmConfig: TfmConfig;

implementation

uses MatrixForm;

{$R *.dfm}

procedure TfmConfig.FonteClick(Sender: TObject);
begin
  FontDialog1.Font.Assign(Matrix2.Font);
  if FontDialog1.Execute then
    Matrix2.Font.Assign(FontDialog1.Font);
end;

procedure TfmConfig.FormShow(Sender: TObject);
begin
  FontDialog1.Font.Assign(fmMatrix.Matrix1.Font);
  ColorBox1.Selected:= fmMatrix.Matrix1.Color;
  ScrollBar1.Position:= fmMatrix.Timer1.Interval div 10;


  fmMatrix.PreencheMatrix(Matrix2);
end;

procedure TfmConfig.btCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TfmConfig.ColorBox1Change(Sender: TObject);
begin
  Matrix2.Color:= ColorBox1.Selected;
end;

procedure TfmConfig.btAplicarClick(Sender: TObject);
begin
  fmMatrix.Matrix1.Font.Assign(Matrix2.Font);
  fmMatrix.Matrix1.Color:= Matrix2.Color;
  fmMatrix.Timer1.Interval:= ScrollBar1.Position * 10;

  if Sender = btOK then
    Close;
end;

end.

object fmMatrix: TfmMatrix
  Left = 142
  Top = 154
  BorderStyle = bsNone
  Caption = 'fmMatrix'
  ClientHeight = 446
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Matrix1: TStringGrid
    Left = 0
    Top = 0
    Width = 688
    Height = 446
    Align = alClient
    BorderStyle = bsNone
    Color = clBlack
    ColCount = 50
    DefaultColWidth = 24
    FixedCols = 0
    RowCount = 50
    FixedRows = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goRangeSelect]
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 0
    OnDblClick = Matrix1DblClick
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 168
    Top = 408
  end
end

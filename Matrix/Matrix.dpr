program Matrix;

uses
  Forms,
  MatrixForm in 'MatrixForm.pas' {fmMatrix},
  Config in 'Config.pas' {fmConfig};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMatrix, fmMatrix);
  Application.CreateForm(TfmConfig, fmConfig);
  Application.Run;
end.
